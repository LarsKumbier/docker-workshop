# Docker Workshop #

Docker Workshop by Lars Kumbier at the DevOpenSpace Leipzig 2015.


## What is this repository for? ##

Here are some examples that we used in the Docker Workshop. Here are the sources:

* [My Docker Server Image](https://seafile.kumbier.it/d/15baa228f6/)
* [Docker Compose](https://docs.docker.com/compose/)
* [Docker Workflow](http://anandmanisankar.com/posts/docker-container-nginx-node-redis-example/)
* [Monitoring](https://medium.com/@chamerling/getting-metrics-from-your-express-app-without-effort-with-docker-and-grafana-ac8f6c42cbfb)

Further links we covered in the addendum:

* [Docker Toolbox](https://www.docker.com/toolbox) (broken at time of writing due to incompatibilities)
* Automatic Docker Cloud via [Docker Swarm](https://www.docker.com/docker-swarm)



## Who do I talk to? ##

You can contact me for Docker themes - or any other topics I do consulting in - via [my homepage](http://kumbier.it)